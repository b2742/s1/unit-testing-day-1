const express = require('express');
const app = express();

app.use(express.json());

const newUser = {
  firstName: 'John',
  lastName: 'Dela Cruz',
  age: 18,
  contactNumber: '09123456789',
  batchNumber: 151,
  email: 'john.delacruz@gmail.com',
  password: 'sixteencharacters'
}

module.exports = {
  newUser: newUser
}

app.listen(process.env.PORT || 5000, () => {
  console.log(`Running at port ${process.env.PORT || 5000}`)
})