const { assert } = require('chai');
const { newUser } = require('../index.js');

//describe gives structure to your test suite.
describe('Test newUser object', ()=> {
	it('Assert newUser type is object', () => {
		assert.equal(typeof(newUser), 'object')
	});
	it('Assert newUser.email is type string', () => {
		assert.equal(typeof(newUser.email), 'string')
	});
	it('Assert newUser.email is not undefined', () => {
		assert.notEqual(typeof(newUser.email), 'undefined')
	});
	it('Assert newUser.password is type string', () => {
		assert.equal(typeof(newUser.password), 'string')
	});
	it('Assert newUser.password is atleast 16 characters', () => {
		assert.isAtLeast(newUser.password.length, 16)
	});
	it('Assert that newUser firstName type is string', () => {
		assert.equal(typeof(newUser.firstName), 'string')
	});
	it('Assert that newUser lastName type is string', () => {
		assert.equal(typeof(newUser.lastName), 'string')
	});
	it('Assert that newUser firstName type is not undefined', () => {
		assert.notEqual(typeof(newUser.firstName), 'undefined')
	});
	it('Assert that newUser lastName type is not undefined', () => {
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	});
	it('Assert that newUser age is atleast 18', () => {
		assert.isAtLeast((newUser.age), 18)
	});
	it('Assert that newUser age type is number', () => {
		assert.equal(typeof(newUser.age), 'number')
	});
	it('Assert that newUser contact number type is string', () => {
		assert.equal(typeof(newUser.contactNumber), 'string')
	});
	it('Assert that newUser batch number type is number', () => {
		assert.equal(typeof(newUser.batchNumber), 'number')
	});
	it('Assert that newUser batch number type is not undefined', () => {
		assert.notEqual(typeof(newUser.batchNumber), 'undefined')
	});
});